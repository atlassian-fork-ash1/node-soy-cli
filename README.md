# Node.js: Atlassian Soy CLI

`atlassian-soy-cli` is a Node wrapper for [Atlassian Soy Templates CLI][atlassian-soy-templates]. 

## Why?

Compile Soy to Javascript as part of your frontend build using frontend tools, not Maven and Java. Easily integrate
with Gulp, Karma, and others. 

## Installation

    npm install --save-dev atlassian-soy-cli

## Usage

`atlassian-soy-cli` exposes a factory function that takes an options object.

    var soyCli = require('atlassian-soy-cli');
    var cli = soyCli({
      version: '4.1.2', // optional
      basedir: 'src/main/resources',
      outdir: 'target/classes',
      type: 'js' // optional
    });

From then on, call `compile(glob)` to compile Soy files that match the glob. All methods support both callback and
promise style.

    // compile all soy templates in basedir (using callback style) 
    cli.compile('**/*.soy', callback)
    
    // compile a single soy template (using promise style)
    cli.compile('myTemplate.soy').then(...)

## Options

The `basedir` and `outdir` options are mandatory. Arguments passed in the `extraArgs` option are passed through to the Soy CLI. 

    {
      version: "4.1.2", // optional
      basedir: "/path/to/soy/templates",
      outdir: "/path/to/compiled/js",
      type: 'js', // optional
      extraArgs: {
        'use-ajs-context': true,
        'rootnamespace': 'mynamespace',
        'i18n': [
          '/path/to/file1',
          '/path/to/file2'
        ]
      }
    }
    
The above `extraArgs` will append the following arguments to the Soy CLI invocation.

    --use-ajs-context --rootnamespace "mynamespace" --i18n "/path/to/file1" --i18n "/path/to/file2"

 [atlassian-soy-templates]: https://bitbucket.org/atlassian/atlassian-soy-templates
 
